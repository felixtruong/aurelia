import * as path from 'path';
import * as webpack from 'webpack';
import * as HtmlWebpackPlugin from 'html-webpack-plugin';

const outDir = path.resolve(__dirname, 'dist');
const srcDir = path.resolve(__dirname, 'src');
const title = "Aurelia"

export default (env = {}): webpack.Configuration =>  ({
    resolve: {
      extensions: ['.ts', '.js'],
      modules: [srcDir, 'node_modules']
    },
    entry: './src/index.ts',
    output: {
      path: outDir,
      publicPath: '/',
      filename: '[name].[hash].bundle.js'
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: 'index.ejs',
        metadata: {
          title
        }
      })
    ]
});
