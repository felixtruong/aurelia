module.exports = {
  singleQuote: true,
  eslintIntegration: true,
  trailingComma: "es5"
};
